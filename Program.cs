﻿// Written by M.A. Mendoza

using System;
using System.IO;
using System.Threading;
using System.Linq;
using System.Collections.Generic;

namespace LyricViewer
{
    class Program
    {
        public const string TerminalLogo = @"

  ,                                                
,-|-. ,-    |               |              |    -. 
`-|-. | ,---|,---.,---.,-.-.|--- ,---.,---.|---. | 
  | |-: |   ||   ||   || | ||    |---'|    |   | :-
`-|-' | `---'`---'`---'` ' '`---'`---'`---'`   ' | 
  `   `-                                        -' 

";
        public const string SectionBreak = "------------";
        public const int LongWait = 2 * 1000;

        public static bool OutputLineByLine; // if True, each line is output only when user presses a key
        public static string[] Files;

        static void Main(params string[] args)
        {
            Console.WriteLine(TerminalLogo);
            TypewriterWait(LongWait);

            string filepath = string.Empty;
            if (args.Length > 0)
            {
                // Get args
                filepath = args[0]; // filepath

                if (args.Length > 1 && args[1] != null)
                {
                    bool.TryParse(args[1].ToString(), out OutputLineByLine);
                }
            }

            if (!string.IsNullOrEmpty(filepath) && File.Exists(filepath))
            {

                Files = new string[]
                {
                    filepath
                };
            }
            else
            {
                LoadFilesInBaseDirectory();
            }

            if (Files.Length < 1)
            {
                Console.WriteLine("No lyric files found. Exiting");
                Console.ReadLine();
                return;
            }

            bool continueToNext = true;
            for(int index = 0; index < Files.Length; index++)
            {
                if(!continueToNext)
                    break;

                // Read file contents into memory
                Console.WriteLine(SectionBreak);
                Console.WriteLine();
                string[] fullLyrics = File.ReadAllText(Files[index]).Split(new [] { '\r', '\n' }, StringSplitOptions.None);

                foreach (string line in fullLyrics)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        // Skip to next line with text
                        Console.WriteLine();
                    }
                    else
                    {
                        char[] characters = line.ToCharArray();
                        foreach (char letter in characters)
                        {
                            Console.Write(letter);
                            TypewriterWait();
                        }
                        Console.WriteLine();
                        LineReadWait();
                    }
                }

                Console.WriteLine(SectionBreak);
                Console.WriteLine();
                if (index < (Files.Length - 1))
                {
                    Console.WriteLine("Press Enter to Continue to next Song.");
                    Console.WriteLine("Or Press any other key to exit.");

                    ConsoleKeyInfo choiceKey = Console.ReadKey();
                    char choice = choiceKey.KeyChar;
                    continueToNext = choice == (char)ConsoleKey.Enter;
                }
            }

            Console.WriteLine(SectionBreak);
            Console.WriteLine($"...{Environment.NewLine}");
        }

        private static void LoadFilesInBaseDirectory()
        {
            List<string> filesFound = Directory.GetFiles($"{AppDomain.CurrentDomain.BaseDirectory}/lyrics")
                .OrderBy(s => s)
                .ToList();

            List<string> acceptedFiles = new List<string>();
            foreach(string file in filesFound)
            {
                if(file.Split(".").Last() == "txt")
                {
                    acceptedFiles.Add(file);
                }
            }
            Files = acceptedFiles.ToArray();
        }

        private static void LineReadWait(int waitTimeInMilliseconds = 2000)
        {
            if (OutputLineByLine)
            {
                Console.ReadKey();
            }
            else
            {
                Thread.Sleep(waitTimeInMilliseconds);
            }
        }

        private static void TypewriterWait(int waitTimeInMilliseconds = 100)
        {
            Thread.Sleep(waitTimeInMilliseconds);
        }
    }
}
